import { Exercise } from './exercie.model';
import { Subject, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { UIService } from '../shared/ui.service';

@Injectable()
export class TrainingService {
    exerciseChanged = new Subject<Exercise>();
    exercisesChanged = new Subject<Exercise[]>();
    finishedExercisesChanged = new Subject<Exercise[]>();
    private availableExercises: Exercise[] = [];
    private firebaseSubscriptions: Subscription[] = [];

    private runningExercise: Exercise;

    constructor(private firestore: AngularFirestore, private uiService: UIService) {}

    fetchAvailableExercises() {
        this.uiService.loadingStateChanged.next(true);
        this.firebaseSubscriptions.push(
            this.firestore.collection('availableExercises')
                .snapshotChanges()
                    .pipe(
                        map(data => {
                            return data.map(doc =>{
                                return {
                                    id: doc.payload.doc.id,
                                    ...doc.payload.doc.data()
                                }
                            })
                        })
                    )
                .subscribe((exercises: Exercise[])=>{
                    this.availableExercises = exercises;
                    this.exercisesChanged.next([...this.availableExercises]);
                    this.uiService.loadingStateChanged.next(false);
                }, err => {
                    this.uiService.loadingStateChanged.next(false);
                    this.uiService.showSneakbar('Fetching exercises failed.');
                    this.exercisesChanged.next(null);
                })
        );
    }

    startExercise(selectedId: string) {
        const selectedExercise = this.availableExercises.find(ex => ex.id === selectedId);
        this.runningExercise = selectedExercise;
        this.exerciseChanged.next({...this.runningExercise});
    }

    completeExercise() {
        this.addDataToDatabase({...this.runningExercise, date: new Date(), state: 'complete'});
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    cancelExercise(progress: number) {
        this.addDataToDatabase({
            ...this.runningExercise, 
            duration: this.runningExercise.duration * (progress/100), 
            calories: this.runningExercise.calories * (progress/100), 
            date: new Date(), 
            state: 'cancelled'
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    getRunningExercise() {
        return {...this.runningExercise};
    }

    fetchCompletedOrCanceledExercises() {
        this.firebaseSubscriptions.push(
            this.firestore.collection('finishedExercises').valueChanges()
            .subscribe((exercises: Exercise[]) => {
                this.finishedExercisesChanged.next(exercises);
            })
        );
    }

    cancelSubscriptions() {
        this.firebaseSubscriptions.forEach(subscription => subscription.unsubscribe());
    }

    private addDataToDatabase(exercise: Exercise){
        this.firestore.collection('finishedExercises').add(exercise);
    }
}