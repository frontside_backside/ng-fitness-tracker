import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastTrainigsComponent } from './past-trainigs.component';

describe('PastTrainigsComponent', () => {
  let component: PastTrainigsComponent;
  let fixture: ComponentFixture<PastTrainigsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastTrainigsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastTrainigsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
