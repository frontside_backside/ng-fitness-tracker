import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { TrainingRoutingModule } from './training-routing.module';

import { TrainingComponent } from './training.component';
import { CurrentTrainigComponent } from './current-trainig/current-trainig.component';
import { NewTrainigComponent } from './new-trainig/new-trainig.component';
import { PastTrainigsComponent } from './past-trainigs/past-trainigs.component';
import { StopTrainingComponent } from './current-trainig/stop-training.component';

@NgModule({
    declarations: [
        TrainingComponent,
        CurrentTrainigComponent,
        NewTrainigComponent,
        PastTrainigsComponent,
        StopTrainingComponent
    ],
    imports: [
        SharedModule,
        TrainingRoutingModule
    ],
    exports: [],
    entryComponents: [StopTrainingComponent]
})
export class TrainingModule {

}