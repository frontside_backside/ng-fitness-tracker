import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

import { AuthData } from './auth-data.model';
import { TrainingService } from '../training/training.service';
import { UIService } from '../shared/ui.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../app.reducer';

@Injectable()
export class AuthService {
    authChange = new Subject<boolean>();
    isAuthenticated = false;

    constructor(private router: Router, 
        private fireAuth: AngularFireAuth, 
        private trainingService: TrainingService,
        private uiService: UIService,
        private store: Store<{ui: fromApp.State}>
        ) {}

    initAuthListener() {
        this.fireAuth.authState.subscribe(user => {
            if (user){
                this.isAuthenticated = true;           
                this.authChange.next(true);
                this.router.navigate(['/training']);
            } else {
                this.trainingService.cancelSubscriptions();
                this.authChange.next(false);
                this.router.navigate(['/login']);
                this.isAuthenticated = false;
            }
        });
    }

    registerUser(authData: AuthData) {
        this.uiService.loadingStateChanged.next(true);
        // this.store.dispatch({type: 'START_LOADING'});
        this.fireAuth.auth.createUserWithEmailAndPassword(authData.email, authData.password)
        .then(result => {
            this.uiService.loadingStateChanged.next(false);  
            // this.store.dispatch({type: 'STOP_LOADING'});
        })
        .catch(err => {
            this.uiService.loadingStateChanged.next(false);   
            // this.store.dispatch({type: 'STOP_LOADING'});
            this.uiService.showSneakbar(err);
        });
    }

    login(authData: AuthData) {
        this.uiService.loadingStateChanged.next(true);
        // this.store.dispatch({type: 'START_LOADING'});
        this.fireAuth.auth.signInWithEmailAndPassword(authData.email, authData.password)
        .then(result => {
            this.uiService.loadingStateChanged.next(false);
            // this.store.dispatch({type: 'STOP_LOADING'});
        })
       .catch(err => {
            this.uiService.loadingStateChanged.next(false);    
            // this.store.dispatch({type: 'STOP_LOADING'});
            this.uiService.showSneakbar(err);
        });
    }

    logout() {
        this.fireAuth.auth.signOut();
    }

    isAuth() {
        return this.isAuthenticated;
    }
}